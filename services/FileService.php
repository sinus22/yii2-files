<?php

namespace thyseus\files\services;

use thyseus\files\models\File;
use Yii;

class FileService
{

    public function ensureTargetDirectoryExists(string $directory): void
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    /**
     * Move the given file from the protected to the public uploads/ folder.
     * Also set the 'public' attribute to 1.
     *
     * Use protectFile to do this the other way around.
     *
     * @param File $file the file to be moved
     * @see Module.php:uploadPath
     * @see Module.php:uploadPathPublic
     * @return File given file to allow method chaining
     */
    public function publishFile(File $file): File
    {
        $basename = basename($file->filename_path);

        $target_directory = sprintf('%s/%d/',
            Yii::$app->getModule('files')->uploadPathPublic,
            $file->created_by ?? '0');

        $this->ensureTargetDirectoryExists($target_directory);

        $target_path =  $target_directory . $basename;

        rename ($file->filename_path, $target_path);

        $file->updateAttributes([
            'public' => 1,
            'filename_path' => $target_path,
            ]);

        return $file;
    }

    /**
     * Move the given file from the public to the protected uploads/ folder.
     * Also set the 'public' attribute to 0.
     *
     * Use publishFile to do this the other way around.
     *
     * @param File $file the file to be moved
     * @see Module.php:uploadPath
     * @see Module.php:uploadPathPublic
     * @return File given file to allow method chaining
     */
    public function protectFile(File $file): File
    {
        $basename = basename($file->filename_path);

        $target_directory = sprintf('%s/%d/',
            Yii::$app->getModule('files')->uploadPath,
            $file->created_by ?? '0');

        $this->ensureTargetDirectoryExists($target_directory);

        $target_path =  $target_directory . $basename;

        rename ($file->filename_path, $target_path);

        $file->updateAttributes([
            'public' => 0,
            'filename_path' => $target_path,
        ]);

        return $file;
    }

}